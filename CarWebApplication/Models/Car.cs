﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Drawing;
using System.Net.NetworkInformation;

namespace CarWebApplication.Models
{
    public class Car
    {
        [Key]
        public int Id { get; set; }
        public string Brand { get; set; }
        public string Color { get; set; }
        public Kuzov Kuzov { get; set; }
        public double Power { get; set; }
    }

    public enum Kuzov
    {
        sedan,cabrio
    }
}

