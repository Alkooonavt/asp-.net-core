﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using CarWebApplication.Models;
using CarWebApplication.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CarWebApplication.Controllers
{
    public class CarController : Controller
    {
        private CarRepository _context;

        public CarController(Context context)
        {
            _context = new CarRepository(context);
        }
        // GET: CarController
        public ActionResult Index()
        {
            return View(_context.GetAll());
        }

        // GET: CarController/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null) return new BadRequestResult();
            var model = _context.Get((int)id);
            if (model == null) return NotFound();
            return View(model);
        }

        // GET: CarController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CarController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Car car)
        {
            try
            {
                _context.Add(car);
                _context.Context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT Cars ON;");
                _context.Save();
                return RedirectToAction(nameof(Index));
            }
            catch 
            {
                return View();
            }
        }

        // GET: CarController/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null) return new BadRequestResult();
            var model = _context.Get((int)id);
            if (model == null) return NotFound();
            return View(model);
        }

        // POST: CarController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Car car)
        {
            try
            {
                var model = _context.Get(id);
                model.Color = car.Color;
                model.Brand = car.Brand;
                model.Kuzov = model.Kuzov;
                model.Power = model.Power;
                _context.Update(model);
                _context.Save();
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: CarController/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null) return new BadRequestResult();
            var model = _context.Get((int)id);
            if (model == null) return NotFound();
            return View(model);
        }

        // POST: CarController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                var model = _context.Get(id);
                _context.Delete(model);
                _context.Save();
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
