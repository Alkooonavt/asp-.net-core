﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using CarWebApplication.Models;

namespace CarWebApplication.Repositories
{
    public class CarRepository : IRepository<Car>
    {
        public Context Context;
        public CarRepository(Context context)
        {
            Context = context;
        }
        public IEnumerable<Car> GetAll()
        {
            return Context.Cars.ToList();
        }

        public Car Get(int id)
        {
            return Context.Cars.Find(id);
        }

        public Car Update(Car entity)
        {
            if (Context.Cars.Find(entity.Id) == null) throw new Exception("Null");
            Context.Update(entity);
            return entity;
        }

        public bool Delete(Car entity)
        {
            if (Context.Cars.Find(entity.Id) == null) return false;
            Context.Cars.Remove(entity);
            return true;
        }

        public Car Add(Car entity)
        {
            Context.Cars.Add(entity);
            return entity;
        }

        public void Save()
        {
            Context.SaveChanges(); 
        }
    }
}