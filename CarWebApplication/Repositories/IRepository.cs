﻿using System.Collections.Generic;

namespace CarWebApplication.Repositories
{
    public interface IRepository<T> where T:class
    {
        IEnumerable<T> GetAll();
        T Get(int id);
        T Update(T entity);
        bool Delete(T entity);
        T Add(T entity);
        void Save();
    }
}