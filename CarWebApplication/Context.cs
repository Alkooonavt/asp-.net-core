﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarWebApplication.Models;
using Microsoft.EntityFrameworkCore;

namespace CarWebApplication
{
    public class Context:DbContext
    {
        public Context(DbContextOptions<Context> options):base(options)
        {
            Database.EnsureCreated();
        }
        public DbSet<Car> Cars { get; set; }
    }
}
